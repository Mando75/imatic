package net.bmuller.imatic

import io.ktor.application.*
import io.ktor.routing.*
import io.ktor.util.*
import net.bmuller.imatic.plugins.configureRouting

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused")
fun Application.module() {
    configureRouting()
}